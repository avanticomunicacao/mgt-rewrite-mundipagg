var config = {
    map: {
        '*': {
            "Magento_SalesRule/js/action/set-coupon-code": 'Avanti_RewriteMundipagg/js/action/set-coupon-code',
            "Magento_SalesRule/js/action/cancel-coupon": 'Avanti_RewriteMundipagg/js/action/cancel-coupon'
        }
    }
};
